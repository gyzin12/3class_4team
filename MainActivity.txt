/***
 이클립스를 통해 생성되는 소스파일들이 src에 저장됨
리소스파일들은 res폴더에 저장됨
main.xml : 최초 액티비티가 어떤 객체들을 갖는지 정의하는 파일
*/
package com.example.taxita;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import java.util.ArrayList;
import android.content.Context;
import android.content.Intent;

public class MainActivity extends Activity {
    private ListView listView;
    private Button makeRoom;
    private ArrayList<String> arrayList;
    private ArrayList<String> sub_arrayList;
    private ArrayAdapter<String> adapter;
    private ArrayAdapter<String> sub_adapter;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Context thisActivity = (Context)this; //현재의 액티비티의 객체 정의
        
        //arrayList 객체 생성, 요소추가
        arrayList = new ArrayList<String>();
        arrayList.add("경복궁 3번 출구");
        arrayList.add("경복궁 2번 출구");
        arrayList.add("괌화문 6번 출구");
        arrayList.add("경복궁 1번 출구");
        arrayList.add("광화문 2번 출구");
        arrayList.add("대학로 4번 출구");
        arrayList.add("대학로 4번 출구");
        arrayList.add("대학로 4번 출구");
        arrayList.add("대학로 4번 출구");
        arrayList.add("대학로 4번 출구");
        arrayList.add("대학로 4번 출구");
        
        //어답터 객체 생성
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);
        sub_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_2, sub_arrayList);
        //리스트뷰 객체 생성, 어댑터 설정
        listView = (ListView)findViewById(R.id.listView1);
        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        
        //리스트뷰에 아이템 클릭 리스너 부착
        listView.setOnItemClickListener(new OnItemClickListener() {
        	public void onItemClick(AdapterView<?> parent, View v, int position, long id){
        		String str = (String)adapter.getItem(position);
        		Toast.makeText(getBaseContext(), str, Toast.LENGTH_SHORT).show();
        		
        		Intent intent1 = new Intent(thisActivity, Room.class);
				startActivity(intent1);
        	}
        } );
        
        //버튼은 새 방 만드는 버튼임
        Button button1= (Button)findViewById(R.id.button1); //버튼 객체 생성
        
        OnClickListener button1ClickListener = new OnClickListener()
        {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent2 = new Intent(thisActivity, MakeRoom.class);
				startActivity(intent2);
			}
        };
        button1.setOnClickListener(button1ClickListener); //메소드실행 
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    } 
}
